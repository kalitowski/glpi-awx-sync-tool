#!/usr/bin/env python

import os
import sys
import argparse
import requests
import operator
import socket

glpi_app_token = os.environ.get("GLPI_APP_TOKEN") 
glpi_user_token = os.environ.get("GLPI_USER_TOKEN") 
glpi_address= 'glpi.bluebrick.work'

try:
    import json
except ImportError:
    import simplejson as json

def is_valid_ipv4_address(address):
    try:
        socket.inet_pton(socket.AF_INET, address)
    except AttributeError:  # no inet_pton here, sorry
        try:
            socket.inet_aton(address)
        except socket.error:
            return False
        return address.count('.') == 3
    except socket.error:  # not a valid address
        return False
    return True


def generate_glpi_session_token():
    r = requests.get(url = "https://" + glpi_address + "/api/initSession?=", headers = {'App-token': glpi_app_token,'Content-Type': 'application/json', 'Authorization': 'user_token ' + glpi_user_token}).json()
    glpi_session_token = r.get('session_token')
    return glpi_session_token

def get_glpi_hosts(param):
    r = requests.get(url = "https://" + glpi_address + "/api/Computer/", headers = {'Content-Type': 'application/json', 'Session-Token': glpi_session_token , 'App-token': glpi_app_token}).json()
    hosts = [x[param] for x in r]
    return hosts

def get_glpi_host_name(param):
    r = requests.get(url = "https://" + glpi_address + "/api/Computer/" + param, headers = {'Content-Type': 'application/json', 'Session-Token': glpi_session_token , 'App-token': glpi_app_token}).json()
    return r["name"]

def get_glpi_host_id(param):
    r = requests.get(url = "https://" + glpi_address + "/api/Computer/", headers = {'Content-Type': 'application/json', 'Session-Token': glpi_session_token , 'App-token': glpi_app_token}).json()
    for i in r:
        if i['name'] == param:
          return i['id']

def get_glpi_hosts_ip(param):
  r= requests.get(url = "https://" + glpi_address + "/api/Computer/" + param + "?with_networkports=1" , headers = {'Content-Type': 'application/json', 'Session-Token': glpi_session_token , 'App-token': glpi_app_token}).json()
  for i in r["_networkports"]['NetworkPortEthernet']:
      if i['name'].startswith('en') or i['name'].startswith('eth'):
        templist=(i['NetworkName']['IPAddress'])
        ips = [x["name"] for x in templist ]
        for j in ips:
            if is_valid_ipv4_address(str(j)):
              response = os.system("ping -c 1 " + j + " > /dev/null &")
              if response == 0:
                ip=j
  if ip is None:
    for i in r["_networkports"]['NetworkPortWifi']:
        templist=(i['NetworkName']['IPAddress'])
        ips = [x["name"] for x in templist ]
        for j in ips:
           if is_valid_ipv4_address(j):
             ip=j
  return ip

def get_username_from_host(param):
    r = requests.get(url="https://" + glpi_address + "/api/Computer/" + param,
                     headers={'Content-Type': 'application/json', 'Session-Token': glpi_session_token,
                              'App-token': glpi_app_token}).json()
    return r['contact']

glpi_session_token = generate_glpi_session_token()
temp_hosts_list=get_glpi_hosts("name")

data = {}
data['group'] = {'hosts':[],'vars':{}}
data['_meta'] = {'hostvars':{}}
for i in temp_hosts_list:
  #  i = i + '.bluebrick.work'
    data['group']['hosts'].append(i)
    data['_meta']['hostvars'][i] = {"ansible_host": get_glpi_hosts_ip(str(get_glpi_host_id(i))),"ansible_ssh_user": get_username_from_host(str(get_glpi_host_id(i)))}

def main():
    print(json.dumps(data, sort_keys=True, indent=2))

if __name__ == '__main__':
    main()
